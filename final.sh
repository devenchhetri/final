#! /bin/bash

#It shows help inforamtion on how to run the script

 echo "Please touch either created_userfile.txt or deleted_userfile.txt"
 echo "Make sure they are in /root directory"
 echo "They should contain users to be created or deleted"

#Y implies "created_userfile.txt" containing users to be created
 Y="$(cat /root/created_userfile.txt)"

#Z implies "deleted_userfile" containing users to be deleted
 Z="$(cat /root/deleted_userfile.txt)"

  previous_entries="$(cat /etc/passwd | cut -f1,3 -d: | sort)"
  username1="$(echo "$previous_entries" | cut -f1 -d:)"
  uid1="$(echo "$previous_entries" | cut -f1 -d:)"
    

#It creates users within the file "created_userfile.txt"

if [ -n "$Y" ]; then
  for i in ${Y[@]}; do
    useradd $i 
    echo $i:$i"20" | chpasswd
  done
fi


#It deletes users within the file "deleted_userfile.txt"
  
if [ -n "$Z" ]; then
  for j in ${Z[@]}; do
    userdel -r $j
  done 
fi


#Below shows comparison before and after users creation or deletion

while true; do
  sleep 3
  current_entries="$(cat /etc/passwd | cut -f1,3 -d: | sort)"
  

  created_users="$(comm -13 <(echo "$previous_entries") <(echo "$current_entries"))"
  username2="$(echo "$created_users" | cut -f1 -d:)"
  uid2="$(echo "$created_users" | cut -f2 -d:)"

  if [ -n "$username2" ]; then
      echo "[+] User "$username2" with UID "$uid2" was created"
  fi
   
  
  deleted_users="$(comm -23 <(echo "$previous_entries") <(echo "$current_entries"))"
  username3="$(echo "$deleted_users" | cut -f1 -d:)"
  uid3="$(echo "$deleted_users" | cut -f2 -d:)"

  if [ -n "$username3" ]; then
      echo "[-] User "$username3" with UID "$uid3" was deleted"
  fi
   

  previous_entries="$(echo "$current_entries")"
  
  echo "scripting is success"
done
